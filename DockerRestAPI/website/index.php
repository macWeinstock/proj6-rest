<html>
    <head>
        <title>CIS 322 REST-api: Brevets</title>
    </head>

    <body>
        <h1>Brevets DB</h1>

        <!-- Got style and formatting from here: https://www.w3schools.com/howto/howto_css_two_columns.asp -->

        <style>
            {
                box-sizing: border-box;
            }

            /* Create two equal columns that floats next to each other */
            .column {
              float: left;
              width: 50%;
              padding: 10px;
              height: 300px; /* Should be removed. Only for demonstration */
            }

            /* Clear floats after the columns */
            .row:after {
              content: "";
              display: table;
              clear: both;
            }
        </style>

        <div class="row">
            <div class="column" style="background-color:#aaa;">
                <h2>All times (JSON)</h2>
                <ul>
                    <?php
                        $json = file_get_contents('http://laptop-service:5000/listAll');
                        $obj = json_decode($json);
                        $items = $obj->items;

                        echo "<p style=color:#009900;>Open:</p>";
                        foreach($items->open as $i){
                            echo "<li>$i</li>";
                        }

                        echo "<p style=color:#cc0000;>Close:</p>";
                        foreach($items->close as $i){
                            echo "<li>$i</li>";
                        }
                    ?>
                </ul>
            </div>
      
            <div class="column" style="background-color:#bbb;">
                <h2>Open only (JSON)</h2>
                    <?php
                        $json = file_get_contents('http://laptop-service:5000/listOpenOnly');
                        $obj = json_decode($json);
                        $items = $obj->items;

                        echo "<p style=color:#009900;>Open:</p>";
                        foreach($items->open as $i){
                            echo "<li>$i</li>";
                        }
                    ?>
            </div>

            <div class="column" style="background-color:#aaa;">
                <h2>All times (CSV)</h2>
                    <?php
                        $csv = file_get_contents('http://laptop-service:5000/listAll/csv');
                        echo "<p>$csv<p>";

                        echo "Sorry, not formatted nicely but all the data is there..."
                    ?>
            </div>

            <div class="column" style="background-color:#bbb;">
                <h2>Close only (CSV)</h2>
                    <?php
                        $csv = file_get_contents('http://laptop-service:5000/listCloseOnly/csv');
                        echo "<p style=color:#cc0000;>Close:</p>";
                        echo "<li>$csv</li>";
                    ?>
            </div>

            <div class="column" style="background-color:#aaa;">
                <h2>Top 2 open times (JSON)</h2>
                <ul>
                    <?php
                        $json = file_get_contents('http://laptop-service:5000/listOpenOnly/json?top=2');
                        $obj = json_decode($json);
                        $items = $obj->items;

                        echo "<p style=color:#009900;>Open:</p>";
                        foreach($items->open as $i){
                            echo "<li>$i</li>";
                        }
                    ?>
                </ul>
            </div>
        </div>

        
    </body>
</html>