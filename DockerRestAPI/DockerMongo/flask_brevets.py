"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
from flask_restful import Resource, Api

import logging

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
#Mongo Client
###

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    db.tododb.drop()
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_time = request.args.get('begin_time')
    begin_date = request.args.get('begin_date')
    brevet_dist_km = request.args.get("brevet_dist_km")

    #Check for valid distances in the acp_times file and return an "invalid"
    #JSON object if returns false. Otherwise continue.
    if not acp_times.check_valid_distances(km, brevet_dist_km):
        result = {"invalid": km}
        return flask.jsonify(result=result)

    #Got the following line from piazza @87
    date_time_string = begin_date + ' ' + begin_time + ':00'

    open_time = acp_times.open_time(km, brevet_dist_km, date_time_string)
    close_time = acp_times.close_time(km, brevet_dist_km, date_time_string)

    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


@app.route('/_submit', methods=['GET','POST'])
def submit():

    o = request.form['open'].replace("[", "").replace("]", "")
    o = o.split(',')

    c = request.form['close'].replace("[", "").replace("]", "")
    c = c.split(',')

    if not o or not c:
        return "ERROR: null entry found"

    result = { "open" : o,
               "close" : c }
 
    db.tododb.insert_one(result)

    return "OK"
    

@app.route('/_display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return flask.render_template('display.html', items=items)

class listAll(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]

        if items[0]:
            items[0].pop('_id')
        else:
            return "empty"

        items1 = {
            "open" : items[0]['open'],
            "close" : items[0]['close']
        }

        return flask.jsonify(items=items1)

class listAllJSON(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]

        if items[0]:
            items[0].pop('_id')
        else:
            return "empty items"

        items1 = {
            "open" : items[0]['open'],
            "close" : items[0]['close']
        }

        return flask.jsonify(items=items1)


class listAllCSV(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]

        if items[0]:
            items[0].pop('_id')
        else:
            return "empty items"

        close = str(items[0]['close'])
        close = close.replace("\"", " ").replace("[", "").replace("]","").replace("\\", "")

        open1 = str(items[0]['open'])
        open1 = open1.replace("\"", " ").replace("[", "").replace("]","").replace("\\", "")

        items1 = ["close", close, "open", open1]
            
        return items1


class listOpenOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]

        if items[0]:
            items[0].pop('_id')
        else:
            return "empty"

        items1 = {
            "open" : items[0]['open']
        }

        return flask.jsonify(items=items1)

class listOpenOnlyJSON(Resource):
    def get(self):
        top = request.args.get('top')

        _items = db.tododb.find()
        items = [item for item in _items]
        
        if items[0]:
            items[0].pop('_id')
        else:
            return "empty items"


        if top:
            open1 = items[0]['open']
            open1 = open1[:int(top)]
            items1 = {
                "open" : open1
            }
        else:
            open1 = items[0]['open']
            items1 = {
                "open" : open1
            }

        return flask.jsonify(items=items1)


class listOpenOnlyCSV(Resource):
    def get(self):
        top = request.args.get('top')

        _items = db.tododb.find()
        items = [item for item in _items]

        if items[0]:
            items[0].pop('_id')
        else:
            return "empty items"

        if top:
            open1 = items[0]['open']
            open1 = open1[:int(top)]
            items1 = ["open", open1]
        else:
            open1 = str(items[0]['open'])
            open1 = open1.replace("\"", " ").replace("[", "").replace("]","").replace("\\", "")
            items1 = ["open", open1]
    
        return items1


class listCloseOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]

        if items[0]:
            items[0].pop('_id')
        else:
            return "empty"

        items1 = {
            "close" : items[0]['close']
        }

        return flask.jsonify(items=items1)


class listCloseOnlyJSON(Resource):
    def get(self):
        top = request.args.get('top')

        _items = db.tododb.find()
        items = [item for item in _items]
        
        if items[0]:
            items[0].pop('_id')
        else:
            return "empty items"


        if top:
            close = items[0]['close']
            close = close[:int(top)]
            items1 = {
                "close" : close
            }
        else:
            close = items[0]['close']
            items1 = {
                "close" : close
            }

        return flask.jsonify(items=items1)


class listCloseOnlyCSV(Resource):
    def get(self):
        top = request.args.get('top')

        _items = db.tododb.find()
        items = [item for item in _items]

        if items[0]:
            items[0].pop('_id')
        else:
            return "empty items"

        if top:
            close = items[0]['close']
            close = close[:int(top)]
            items1 = ["close", close]
        else:
            close = str(items[0]['close'])
            close = close.replace("\"", " ").replace("[", "").replace("]","").replace("\\", "")
            items1 = ["close", close]
    
        return items1



#JSON resources
api.add_resource(listAll, '/listAll/')
api.add_resource(listOpenOnly, '/listOpenOnly/')
api.add_resource(listCloseOnly, '/listCloseOnly/')
api.add_resource(listAllJSON, '/listAll/json')
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/json')
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/json')

#csv resources
api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')



#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
